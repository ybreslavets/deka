
FROM php:7.0.4-fpm

    RUN apt-get update && apt-get install -y libmcrypt-dev \
        mysql-client libmagickwand-dev --no-install-recommends \
        && pecl install imagick \
        && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
        && docker-php-ext-enable imagick \
        && docker-php-ext-install gd mcrypt pdo_mysql mbstring mysqli zip
        

    WORKDIR /var/www